<?php

class sqlGeneratorCWR {

	protected $recipes;
	protected $workingSet;
	protected $link;
	protected $stack;
	protected $insertCache;

	const IC_RECIPE_CATEGORY 	= 1;
	const IC_INGREDIENT 		= 2;
	const IC_INREDIENT_CATEGORY = 3;

	public function __construct($recipes) {
		$this->recipes = $recipes;
		$this->link = linkHolder::getLink();
		$this->workingSet['cStack'] = [];
		$this->workingSet['devId'] = NULL;
	}

	public function generateSql() {
		$this->workingSet['cStack'][] = 'START TRANSACTION';
		foreach ($this->recipes as 	$device => $recipes) {
			$this->workingSet['devId'] = NULL;
			if(!$this->checkDevice($device))
				continue; //add skip device method
			$this->workingSet['devId'] = $this->getDeviceIdByName($device);
			$this->processRecipes($recipes);
		}
		//var_dump($this->insertCache);die();
		$this->workingSet['cStack'][] = 'COMMIT';
		return $this->workingSet['cStack'];
	}

	protected function processRecipes($recipes) {
		foreach ($recipes as $recipe) {
			$this->processLanguageAsRecipe($recipe);;
		}

	}

	protected function processLanguageAsRecipe($recipe) {
		$imageId = $this->getImageByName($recipe['image']);
		foreach ($recipe['name'] as $language => $recipeName) {
			$time = $recipe['time'];
			$this->prepareRecipeCategory($recipe['category'][$language], $language);
			$recDescription = mysqli_real_escape_string($this->link, $recipe['desc'][$language]);
			$this->workingSet['cStack'][] = "INSERT INTO recipe (lang, cooktime, kkal, category_id, image_id, name)
			VALUES ('$language', '{$recipe['time']}' , '{$recipe['kkal']}', @RCID, '$imageId', '$recipeName')";
			$this->workingSet['cStack'][] = "SET @RID = last_insert_id()";
			$this->workingSet['cStack'][] = "INSERT INTO recipe_desc ( `desc` )
			VALUES ('{$recDescription}')";
			$this->workingSet['cStack'][] = "SET @RDID = last_insert_id()";
			$this->workingSet['cStack'][] = "INSERT INTO recipe_recipe_desc_rel (device_id, recipe_id, desc_id)
			VALUES ('{$this->workingSet['devId']}', @RID, @RDID)";
			$this->workingSet['cStack'][] = "INSERT INTO device_to_recipes ( device_id, recipe_id )
					VALUES ('{$this->workingSet['devId']}', @RID)";
					$this->workingSet['cStack'][] = "INSERT INTO info (recipe_id, time, temperature, sync_version)
					VALUES (@RID, '{$recipe['time']}', '{$recipe['temperature']}', 3)";
					$this->processIngredients($recipe['ingredients'], $language);
		}
	}

	protected function processIngredients($ingredients, $language) {
		foreach ($ingredients as $ingredient) {
			$this->prepareIngredientCategory($ingredient['category'][$language], $language);
			$this->prepareIngredient($ingredient['name'][$language], $ingredient['amount_unit'][$language], $language);
			$this->workingSet['cStack'][] = "INSERT INTO ingredientsquantity (lang, quantity, ingredient, recipe)
			VALUES ('{$language}', '{$ingredient['amount']}', @IID, @RID)";
		}
	}

	protected function prepareIngredient($ingredient, $unit, $language) {
	$ingredient = mysqli_real_escape_string($this->link, $ingredient);
	$unit = mysqli_real_escape_string($this->link, $unit);
	$sql = "SELECT id FROM ingredient WHERE category_id = @ICID AND lang = '{$language}' AND name = '{$ingredient}'";
	$res = mysqli_query($this->link, $sql);
	if(mysqli_num_rows($res) > 0)
		return $this->workingSet['cStack'][] = "SET @IID = '".mysqli_fetch_array($res)[0]."'";
				if($this->inInsertCache($ingredient, $language, self::IC_INGREDIENT))
				return $this->workingSet['cStack'][] = "SET @IID = (SELECT id FROM ingredient WHERE category_id = @ICID AND lang = '{$language}' AND name LIKE '%{$ingredient}%' LIMIT 1)";
				$this->workingSet['cStack'][] = "INSERT INTO ingredient ( lang, category_id, name, quantityunit )
						VALUES ( '{$language}', @ICID, '{$ingredient}', '{$unit}')";
						$this->workingSet['cStack'][] = "SET @IID = last_insert_id()";
						$this->insertCachePush($ingredient, $language, self::IC_INGREDIENT);
						return TRUE;
}

protected function prepareIngredientCategory($category, $language) {
$category = mysqli_real_escape_string($this->link, $category);
$sql = "SELECT id FROM ingredientscategory WHERE name LIKE '%{$category}%' AND lang = '{$language}'";
$res = mysqli_query($this->link, $sql);
if(mysqli_num_rows($res) > 0)
	return $this->workingSet['cStack'][] = "SET @ICID = '".mysqli_fetch_array($res)[0]."'";
if($this->inInsertCache($category, $language, self::IC_INREDIENT_CATEGORY))
	return $this->workingSet['cStack'][] = "SET @ICID = (SELECT id FROM ingredientscategory WHERE name LIKE '%{$category}%' AND lang = '{$language}' LIMIT 1)";
$this->workingSet['cStack'][] = "INSERT INTO ingredientscategory (lang, name) VALUES ('{$language}', '{$category}')";
$this->workingSet['cStack'][] = "SET @ICID = last_insert_id()";
$this->insertCachePush($category, $language, self::IC_INREDIENT_CATEGORY);
return TRUE;
}

protected function getImageByName($imageName) {
	$imageName = mysqli_real_escape_string($this->link, $imageName);
	$sql = "SELECT id FROM image WHERE name LIKE '%{$imageName}%'";
	$res  = mysqli_query($this->link, $sql);
	if(mysqli_num_rows($res) < 0)
		return 0;
	return mysqli_fetch_array($res)[0];
}



protected function prepareRecipeCategory($category, $language) {
	$category  = mysqli_real_escape_string($this->link, $category);
	$language = mysqli_real_escape_string($this->link, $language);
	if($recipeCategoryId = $this->getRecipeCategoryByName($category, $language))
		return $this->workingSet['cStack'][] = "SET @RCID = $recipeCategoryId";
	if($this->inInsertCache($category, $language, self::IC_RECIPE_CATEGORY) && !$idFoundInDb)
		return $this->workingSet['cStack'][] = "SET @RCID = (SELECT recipecategory.id
		FROM test_cookwithredmond.recipecategory JOIN test_cookwithredmond.recipe ON recipecategory.id = recipe.category_id
		WHERE recipecategory.name LIKE '$category' AND recipe.lang = '{$language}' LIMIT 1)";
		$this->workingSet['cStack'][] = "INSERT INTO recipecategory (name, area_id) VALUES ('$category', '1')";
	$this->workingSet['cStack'][] = "SET @RCID = last_insert_id()";
		$this->insertCachePush($category, $language, self::IC_RECIPE_CATEGORY);
		return TRUE;
}

	protected function getRecipeCategoryByName ($category, $language) {
	$category = mysqli_real_escape_string($this->link, $category);
	$sql = "SELECT id FROM test_cookwithredmond.recipecategory  WHERE name LIKE '%$category%' LIMIT 1";
	$res = mysqli_query($this->link, $sql);
	if(mysqli_num_rows($res) > 0)
		return mysqli_fetch_array($res)[0];
		return FALSE;
	}

	protected function insertCachePush($value, $language, $type) {
	$this->insertCache[$type][$language][strtoupper($value)] = strtoupper($value);
	return TRUE;
	}

	protected function inInsertCache($needle, $language, $type) {
	if(isset($this->insertCache[$type][$language]) && in_array(strtoupper($needle), $this->insertCache[$type][$language]))
		return TRUE;
		return FALSE;
	}

	protected function checkDevice($devName) {
	$name = mysqli_real_escape_string($this->link, $devName);
	$sql = "SELECT count(*) FROM device WHERE name LIKE '%{$name}%'";
	$res = mysqli_query($this->link, $sql);
	$devices = mysqli_fetch_array($res)[0];
	if ($devices != 1)
		return FALSE;
		return TRUE;
	}

		protected function getDeviceIdByName($name) {
		$name = mysqli_real_escape_string($this->link, $name);
		$sql = "SELECT id FROM device WHERE name LIKE '%{$name}%'";
		$res = mysqli_query($this->link, $sql);
		$devId = mysqli_fetch_array($res)[0];
		return $devId;
		}




		}
