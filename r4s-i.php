<?php

function generate_sql_r4s($recipes) {
	$stack  = array();
	$skip = array();
	$stack[][] = "USE $database";
	$failed = array();
	generate_sql_parse_devices($recipes, $stack, $skip);
	$result['stack'] = $stack;
	$result['skip'] = $skip;
	return $result;


}

function generate_sql_parse_devices ($recipes, &$stack, &$skip) {
	$realInsertCache = [];
	$realInsertCache['ingredientscategory'] = [];
	$realInsertCache['recipecategory'] = [];
	$realInsertCache['ingredient'] = [];
	$insertCacheSet = FALSE;
	$insertCacheAddQueue = FALSE;
	$link = linkHolder::getLink();
	foreach ($recipes as $device => $recipesDev) {
		$sql = "SELECT id FROM device WHERE name LIKE '%{$device}%'";
		$res = mysqli_query($link, $sql);
		if (mysqli_num_rows($res) < 1)
			throw new Exception("Device not found!");
		$arr = mysqli_fetch_array($res);
		$tDevId = $arr[0];
		$devId = $arr[0]; //TODO: Raise exception if device not found
		generate_sql_parse_recipes($recipesDev, $devId, $stack, $skip, $realInsertCache);
	}

}

function generate_sql_parse_recipes($recipesDev, $devId, &$stack, &$skipStack, &$realInsertCache) {
	$link = linkHolder::getLink();
	foreach ($recipesDev as $currentRecipe) {
		$cStack = array();
		$cStack[] = 'START TRANSACTION';
		$skipRecipe = NULL;
		$insertCache = $realInsertCache;

		//Process recipe categories
		generate_sql_parse_recipes_categories($currentRecipe, $insertCache);

		//Get image
		$imageId = 0;
		$sql = "SELECT id FROM image WHERE name = '{$currentRecipe['image']}'";
		$res = mysqli_query($link, $sql);
		if(mysqli_num_rows($res) == 1)
			$imageId = mysqli_fetch_array($res)[0];

		//Create recipe
		$cStack[] = "INSERT INTO recipe (cooktime, kkal, category_id, image_id) VALUES ('{$currentRecipe['time']}', '{$currentRecipe['kkal']}', @RCID, '{$imageId}')";
		$cStack[] = "SET @RID = last_insert_id()";

		//Let's search for prgram
		$skipRecipe = generate_sql_parse_programmodes($currentRecipe, $devId, $cStack);

		//fill out recipe names
		echo $currentRecipe['name']['en']."\r\n";
		foreach ($currentRecipe['name'] as $lang => $name) {
			$name = mysqli_real_escape_string($link, $name);
			$cStack[] = "INSERT INTO recipe_lang (pid, lang, attr, value) VALUES ( @RID, '$lang', 'name', '$name')";
		}

		//fill out recipe body
		$cStack[] = "INSERT INTO recipe_desc VALUES ()";
		$cStack[] = "SET @RDID = last_insert_id()";
		foreach ($currentRecipe['desc'] as $lang => $desc) {
			$desc = mysqli_real_escape_string($link, $desc);
			$cStack[] = "INSERT INTO recipe_desc_lang ( pid, lang, attr, value ) VALUES ( @RDID, '$lang', 'desc', '$desc' )";
		}

		//Fill out relation
		$cStack[] = "INSERT INTO recipe_recipe_desc_rel (device_id, recipe_id, desc_id) VALUES ($devId, @RID, @RDID)";
		$cStack[] = "INSERT INTO device_to_recipes (device_id, recipe_id) VALUES ($devId, @RID )";

		//Process ingidients
		foreach ($currentRecipe['ingredients'] as $ingredient) {
			generate_sql_parse_ingredients_category($ingredient, $cStack, $insertCache);
			generate_sql_parse_ingredients ($ingredient, $cStack, $insertCache);
			$cStack[] = "INSERT INTO ingredientsquantity ( quantity, ingredient, recipe ) VALUES ( '{$ingredient['amount']}', @IID, @RID )";
		}
		$cStack[] = 'COMMIT';
		if(is_null($skipRecipe)) {
			$stack[] = $cStack;
			$realInsertCache = $insertCache;
		} else {
			$skip = $currentRecipe;
			$skip['skip'] = $skipRecipe;
			$skipStack[] = $skip;
		}
	}
}

function generate_sql_parse_ingredients($ingredient, &$cStack, &$insertCache) {
	$link = linkHolder::getLink();
	$ingrId = NULL;
	$notFoundIng = NULL;
	$ingrIdFound = FALSE;
	$insertCacheSet = FALSE;
	foreach($ingredient['name'] as $ingLang => $ingName) {
		$sql = "SELECT pid FROM ingredient_lang JOIN ingredient ON ingredient_lang.pid = ingredient.id WHERE lang = '$ingLang' AND value = '$ingName' AND category_id = @ICID";
		$res = mysqli_query($link, $sql);
		//if(mysqli_num_rows($res) > 0) {
		if(FALSE) {
			$arr = mysqli_fetch_array($res);
			$ingrId = $arr[0];
			$ingrIdFound = TRUE;
		} else {
			if(isset($insertCache['ingredient'][$ingLang]) && in_array(strtoupper($ingName), $insertCache['ingredient'][$ingLang])) {
				$insertCacheSet = TRUE;
				$ingrIdQ = "SET @IID = (SELECT pid FROM ingredient_lang WHERE lang = '$ingLang' AND value = '$ingName' LIMIT 1)";
			} else {
				$notFoundIng[$ingLang] = $ingName;
			}
		}
	}

	if($ingrIdFound) {
		$cStack[] = "SET @IID = $ingrId";
	} elseif ($insertCacheSet) {
		$cStack[] = $ingrIdQ;
		$insertCacheSet = FALSE;
	} else {
		$cStack[] = "INSERT INTO ingredient ( category_id ) VALUES ( @ICID )";
		$cStack[] = "SET @IID = last_insert_id()";
	}
	if(sizeof($notFoundIng) > 0 ) {
		foreach ($notFoundIng as $nfilang => $NFIngName) {
			$insertCache['ingredient'][$nfilang][strtoupper($NFIngName)] = strtoupper($NFIngName);
			$NFIngName = mysqli_real_escape_string($link, $NFIngName);
			$cStack[] = "INSERT INTO ingredient_lang (pid, lang, attr, value) VALUES (@IID, '$nfilang', 'name', '$NFIngName')";
			$cStack[] = "INSERT INTO ingredient_lang (pid, lang, attr, value) VALUES (@IID, '$nfilang', 'quantityunit', '{$ingredient['amount_unit'][$nfilang]}')";
		}
	}
}

function generate_sql_parse_ingredients_category($ingredient, &$cStack, &$insertCache) {
	$link = linkHolder::getLink();
	$ingrCatId = null;
	$notFoundIngrCat = null;
	$ingrCatIdFound = FALSE;
	$ingrCatIdQ = FALSE;
	$insertCacheSet = FALSE;
	foreach ($ingredient['category'] as $ICLang => $category) {
		$category = mysqli_real_escape_string($link, $category);
		$sql = "SELECT pid FROM ingredientscategory_lang WHERE lang = '$ICLang' AND value = '$category'";
		$res = mysqli_query($link, $sql);
		//if(mysqli_num_rows($res) > 0) {
		if(FALSE) {
			$arr = mysqli_fetch_array($res);
			$ingrCatId = $arr[0];
			$ingrCatIdFound = TRUE;
		} else {
			if(isset($insertCache['ingredientscategory'][$ICLang]) && in_array(strtoupper($category), $insertCache['ingredientscategory'][$ICLang])) {
				$insertCacheSet = TRUE;
				$ingrCatIdQ = "SET @ICID = (SELECT pid FROM ingredientscategory_lang WHERE lang = '$ICLang' AND value = '$category' LIMIT 1)";
			} else {
				$notFoundIngrCat[$ICLang] = $category;
			}
		}
	}
	if ($ingrCatIdFound) {
		$cStack[] = "SET @ICID = $ingrCatId";
	}elseif ($insertCacheSet) {
		$cStack[] = $ingrCatIdQ;
		$insertCacheSet = FALSE;
	} else {
		$cStack[] = "INSERT INTO ingredientscategory VALUES ()";
		$cStack[] = "SET @ICID = last_insert_id()";
	}
	if(sizeof($notFoundIngrCat) > 0 ) {
		foreach ($notFoundIngrCat as $nficlang => $categoryName) {
			$insertCache['ingredientscategory'][$nficlang][strtoupper($categoryName)] = strtoupper($categoryName);
			$categoryName = mysqli_real_escape_string($link, $categoryName);
			$cStack[] = "INSERT INTO ingredientscategory_lang (pid, lang, attr, value) VALUES (@ICID, '$nficlang', 'name', '$categoryName')";
		}
	}
}

function generate_sql_parse_programmodes($currentRecipe, $devId, &$cStack) {
	$link = linkHolder::getLink();
	$skipRecipe = NULL;
	foreach ($currentRecipe['modes'] as $mode) {
		$modeP = mysqli_real_escape_string($link, $mode['program']);
		$sql = "SELECT program_lang.pid
		FROM program JOIN program_lang ON program.id = program_lang.pid
		WHERE program.device_id = $devId AND program_lang.value LIKE '{$modeP}'";
		$res = mysqli_query($link, $sql);
		if(mysqli_num_rows($res) < 1)
			$skipRecipe = 'PROGRAM';
		$arr = mysqli_fetch_array($res);
		$programId = $arr[0];

		//OK we've got program. Let's search for mode
		if (!is_null($skipRecipe))
			$programId == 0;
		$modeN = mysqli_real_escape_string($link, $mode['mode']);
		$sql = "SELECT mode.id
		FROM mode JOIN mode_lang ON mode.id = mode_lang.pid
		WHERE value LIKE '{$modeN}' AND program_id = '$programId'";
		$res = mysqli_query($link, $sql);
		if(mysqli_num_rows($res) < 1)
			$skipRecipe = 'MODE';
			$arr = mysqli_fetch_array($res);
			$modeId = $arr[0];
			$cStack[] = "INSERT INTO info (mode_id, recipe_id, time, temperature)
			VALUES ('$modeId', @RID, '{$mode['time']}', '{$mode['temperature']}')";
	}
	if(!isset($skipRecipe))
		return NULL;
	return $skipRecipe;
}

function generate_sql_parse_recipes_categories(&$currentRecipe, &$insertCache) {
	$link = linkHolder::getLink();
	$recCatId = NULL;
	$recCatIdQ = NULL;
	$notFoundRecCat = null;
	$recCatIdFound = FALSE;
	$insertCacheSet = FALSE;
	$insertCacheAddQueue = FALSE;

	foreach ($currentRecipe['category'] as $lang => $category) {
		$category = mysqli_real_escape_string($link, $category);
		$sql = "SELECT pid FROM recipecategory_lang WHERE lang = '$lang' AND value = '$category'";
		$res = mysqli_query($link, $sql);
		if(mysqli_num_rows($res) > 0) {
			$arr = mysqli_fetch_array($res);
			$recCatId = $arr[0];
			$recCatIdFound = TRUE;
		} else {
			if(isset($insertCache['recipecategory'][$lang]) && in_array(strtoupper($category), $insertCache['recipecategory'][$lang])) {
				$insertCacheSet = TRUE;
				$recCatIdQ = "SET @RCID = (SELECT pid FROM recipecategory_lang WHERE lang = '$lang' AND value = '$category' LIMIT 1)";
			} else {
				$notFoundRecCat[$lang] = $category;
			}
		}
	}

	if ($recCatIdFound){
		$cStack[] = "SET @RCID = $recCatId";
	} elseif ($insertCacheSet) {
		$cStack[] = $recCatIdQ;
		$insertCacheSet = FALSE;
	} else {
		$cStack[] = "INSERT INTO recipecategory (area_id) VALUES ('1')";
		$recCatId = "SET @RCID = last_insert_id()";
		$cStack[] = $recCatId;
	}

	if(sizeof($notFoundRecCat) > 0 ) {
		foreach ($notFoundRecCat as $lang => $categoryName) {
			$insertCache['recipecategory'][$lang][strtoupper($categoryName)] = strtoupper($categoryName);
			echo 'Added '.$lang.' '.$categoryName."'\r\n";
			$categoryName = mysqli_real_escape_string($link, $categoryName);
			$cStack[] = "INSERT INTO recipecategory_lang (pid, lang, attr, value) VALUES ( @RCID, '$lang', 'name', '$categoryName')";

		}
	}
}
