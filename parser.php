<?php

function parse_csv($path, $delimiter) {
	$fp = fopen($path, "r");
	$keys = fgetcsv($fp,0,$delimiter);
	global $recipes;
	$recipes = [];
	$lastDevice = null;
	$lastRecipe = null;
	$recipe_count = 0;

	$langs = [];
	// Process keys for langs
	$k_copy = $keys;
	unset($k_copy[0]);
	unset($k_copy[3]);

	while(count($k_copy) > 0){
		$s = array_splice($k_copy, 0, 15);
		$lng_name = strtolower(trim(str_replace("��� �����", "", $s[0])));
		if(empty($lng_name)){
			$lng_name = 'en';
		}
		$langs[] = $lng_name;
	}

	// Iterate recipes
	while(($f = fgetcsv($fp,0,"\t")) !== FALSE){
		$isRecipe = (!empty($f[0]) && !empty($f[2]));

		if($isRecipe){
			list($device) = array_splice($f, 0, 1);
			list($image) = array_splice($f, 2, 1);
			$trans = [];
			$recipe = [];
			$key = crc32(json_encode($f));
			$first_mode_set = FALSE;

			// Iterate translations
			while(count($f) > 0){
				$current_lang = $langs[count($trans)];
				$recipe_mock = array_splice($f, 0, 15);

				$recipe_meta = array_splice($recipe_mock, count($recipe_mock)-4, 4);
					
				list($program) = $recipe_meta;
				$recipe['program'] = $program;
				$recipe['time'] = intval($recipe_mock[4]);
				$recipe['temperature'] = (int) $recipe_meta[3];
				$recipe['mode'] = rtrim($recipe_meta[2]);
				if(!$first_mode_set) {
					if(strlen($recipe['mode']) < 1)
						$recipe['mode'] = 'default';
					$recipe['modes'][] = array(
							'program'		=> $recipe['program'],
							'time'			=> $recipe['time'],
							'temperature'	=> $recipe['temperature'],
							'mode'			=> $recipe['mode']
					);
					$first_mode_set = TRUE;
				}
					
					

				unset($recipe_mock[4]);
				unset($recipe_mock[5]);

				list($rcategory) = array_splice($recipe_mock, 0, 1);
					
				//var_dump($recipe_mock); die();
					
				$category = rtrim($rcategory);


				list($rname, $rkkal, $skip, $rdesc) = array_splice($recipe_mock, 0, 4);
				preg_match("/([0-9\,\.]+)$/", rtrim($rkkal), $needles);
				$recipe['name'][$current_lang] = $rname;
				$recipe['category'][$current_lang] = $category;
				$recipe['kkal'] = $needles[0];
				$recipe['kkal'] = (float) str_replace(',', '.', $recipe['kkal']);
				$recipe['desc'][$current_lang] = $rdesc;

				list($icategory, $iname, $iamount, $iamount_unit) = $recipe_mock;

				$recipe['ingredients'][0]['name'][$current_lang] = rtrim($iname);
				$recipe['ingredients'][0]['category'][$current_lang] = $icategory;
				$recipe['ingredients'][0]['amount'] = intval($iamount);
				$recipe['ingredients'][0]['amount_unit'][$current_lang] = $iamount_unit;

				$trans[] = $recipe_mock;
			}
			$recipe['image'] = $image;

			$recipes[$device][$key] = $recipe;
			$lastDevice = $device;
			$lastRecipe  = $key;
			$recipe_count++;
		}else{
			array_splice($f, 0, 1);
			array_splice($f, 2, 1);
			$trans = [];
			$ingredient = [];
			if(strlen($f[11])>0) {
				$recipes[$lastDevice][$lastRecipe]['modes'][] = array(
						'program'		=> $f[11],
						'time'			=> (int) $f[12],
						'mode'			=> (strlen(rtrim($f[13])) < 1) ? 'default' : rtrim($f[13]),
						'temperature'	=> (int) $f[14]
				);
			}
			// Iterate translations
			while(count($f) > 0){
				$current_lang = $langs[count($trans)];
				$recipe_mock = array_splice($f, 0, 15);
				$ingredient_mock = array_splice($recipe_mock, 7, 4);
				$trans[] = $ingredient_mock;
					
				list($icategory, $iname, $iamount, $iamount_unit) = $ingredient_mock;

				$ingredient['name'][$current_lang] = rtrim($iname);
				$ingredient['category'][$current_lang] = $icategory;
				$ingredient['amount'] = intval($iamount);
				$ingredient['amount_unit'][$current_lang] = $iamount_unit;
			}
			$recipes[$lastDevice][$lastRecipe]['ingredients'][] = $ingredient;
		}

	}


	//var_dump($recipe_count, $recipes);
	fclose($fp);

	return $recipes;
}
